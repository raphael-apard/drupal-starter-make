projects[] = geofield
projects[] = geophp
projects[] = leaflet
projects[] = leaflet_more_maps
libraries[leaflet][download][type] = "file"
libraries[leaflet][download][url] = "http://leaflet-cdn.s3.amazonaws.com/build/leaflet-0.7.3.zip"
libraries[leaflet][directory_name] = "leaflet"
libraries[leaflet][type] = "library"
