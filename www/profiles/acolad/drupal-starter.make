; API version
api = 2

; Drupal
core = 7.x
projects[drupal][type] = core
; # Add 'browsers' option to drupal_add_js()
projects[drupal][patch][865536] = "http://www.drupal.org/files/issues/865536-D7-229-PASS.patch"
; # Add consistent sort on path (needed for Global Redirect)
projects[drupal][patch][1160764] = "http://www.drupal.org/files/issues/drupal-1934086-path_load_order-6_0.patch"


;;;;; Utility ;;;;;
projects[ctools][patch][742832] = "http://drupal.org/files/ctools-plugins.inc-load_includes_wrong_caching_algorithm-742832-24.patch"
projects[] = token
projects[] = pathauto
projects[] = transliteration
projects[] = views
projects[] = better_exposed_filters
projects[] = entity
projects[] = entityreference
projects[] = libraries
projects[] = jquery_update
projects[] = l10n_update
projects[] = extlink
projects[] = menu_position
projects[] = smart_trim
projects[] = views_bulk_operations


;;;;; Field ;;;;;
projects[] = field_group
projects[] = addressfield
projects[] = email
projects[] = link


;;;;; SEO ;;;;;
projects[] = globalredirect
;projects[] = redirect
projects[] = metatag
;projects[] = metatags_quick
projects[] = schemaorg
projects[] = xmlsitemap
projects[] = page_title
projects[] = google_analytics
;;; Multisite with differents robotstxt files
;projects[] = robotstxt


;;;;; Content authoring ;;;;;
projects[] = ckeditor
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.5/ckeditor_4.4.5_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
projects[] = ckeditor_link
projects[] = better_formats
;;; Need automatic publications
;projects[] = scheduler
;;; End users knows html ?
;projects[] = ace_editor
;libraries[ace][download][type] = "file"
;libraries[ace][download][url] = "http://github.com/ajaxorg/ace-builds/archive/master.zip"
;libraries[ace][directory_name] = "ace"
;libraries[ace][type] = "library"


;;;;; Media ;;;;;
;projects[media][version] = 2.0-alpha4
;projects[] = file_entity
;projects[] = ckeditor_link_file
;projects[] = focal_point
;;; Need a box ?
; projects[] = colorbox
;libraries[colorbox][download][type] = "file"
;libraries[colorbox][download][url] = "http://github.com/jackmoore/colorbox/archive/1.x.zip"
;libraries[colorbox][directory_name] = "colorbox"
;libraries[colorbox][type] = "library"


;;;;; Responsive ;;;;;
projects[] = breakpoints
projects[] = picture


;;;;; Form ;;;;;
projects[] = webform
projects[] = elements
projects[] = honeypot
;;; End users will create forms
;projects[] = form_builder


;;;;; HTML mail ;;;;;
projects[] = mailsystem
projects[] = mimemail


;;;;; Features ;;;;;
projects[] = features
projects[] = diff
projects[] = strongarm


;;;;; Administration ;;;;;
projects[] = admin_menu
projects[] = adminimal_theme
projects[] = adminimal_admin_menu
;projects[] = shiny

;;;;; Theming ;;;;;
projects[] = gulpifier
libraries[gulpifier][download][type] = "file"
libraries[gulpifier][download][url] = "https://github.com/makinacorpus/drupal-gulpifier/archive/7.x-1.x.zip"
libraries[gulpifier][directory_name] = "gulpifier"
libraries[gulpifier][type] = "module"

;;;;; Development ;;;;;
projects[] = devel


;;;;; Extra ;;;;;

;;; Need to exposed data as XML/JSON/...
;projects[] = views_datasource

;;; Change default admin / user paths
;projects[] = rename_admin_paths

;;; i18n
;includes[] = extra-makes/i18n.make

;;; Perfs
;includes[] = extra-makes/boost.make

;;; Map
;includes[] = extra-makes/map.make
